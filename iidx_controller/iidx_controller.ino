/*
 * -! PLL configuration is really important - misconfiguring could lead to entire board not functioning properly
 */

#ifndef F_CPU
    #define F_CPU 16000000UL
#endif
#include "Descriptor.h"
#include "Key.h"
#include <string.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
//#include "USBAPI.h"

#define B_FALSE 0
#define B_TRUE  1

#define BAUD_RATE(baud_rate) (((float)F_CPU / (16 * (float)baud_rate)) - 1)

#define INFINITE 1

#define RET_OK (0)

#define DELAY_CONST 25

//#define GET_END0_PACKET_SIZE(UECFG1X & ((1 << EPSIZE0) | (1 << EPSIZE1) | (1 << EPSIZE2)))

/* Error Flash codes */
#define GEN_INT_VECT_CODE        1
#define START_OF_FRAME_INT_CODE  2
#define END_OF_RESET_INT_CODE    3
#define UPSTREAM_RESUME_INT_CODE 4
#define END_OF_RESUME_INT_CODE   5
#define WAKEUP_INT_CODE          6
#define SUSPEND_INT_CODE         7

#define COM_INT_VECT_CODE        8
#define RECEIVED_SETUP_INT_CODE  9
#define GET_DESCRIPTOR_CODE      10
#define SET_ADDRESS_CODE         11

/* Need a Queue for user button presses - can't send the button press codes
 to the host whenever we want - need to await IN packets because we are
 a slave to the host... */

/* Let's use a linked list */
typedef struct UserButtonPress {
    struct UserButtonPress* next;
    unsigned char data;
} KeyPress, * pKeyPress;

typedef struct UserButtonPressQueue {
    pKeyPress front;
    pKeyPress back;
} KeyPressQ, * pKeyPressQ;

pKeyPress enqueue(pKeyPressQ* keyPressQueue, unsigned char pressedKey) {
    pKeyPress ret = NULL;
    pKeyPress newNode = NULL;

    if (NULL == keyPressQueue) {
        ret = NULL;
    }
    else {
        /* Make sure the queue exists */
        if (NULL == *keyPressQueue) {
            *keyPressQueue = (pKeyPressQ)calloc(1, sizeof(KeyPressQ));
        }

        if (NULL != *keyPressQueue) {
            /* Create the node */
            newNode = (pKeyPress)calloc(1, sizeof(KeyPress));
            if (NULL != newNode) {
                newNode->data = pressedKey;
                newNode->next = NULL;

                /* Add the node to the back of the lineup */
                (*keyPressQueue)->back->next = newNode;
                (*keyPressQueue)->back = newNode;

                /* If the queue didn't exist, make sure the front is defined */
                if (NULL == (*keyPressQueue)->front) {
                    (*keyPressQueue)->front = newNode;
                }

                ret = newNode;
            }
        }
    }

    return ret;
}

pKeyPress dequeue(pKeyPressQ keyPressQueue) {
    pKeyPress ret = NULL;

    if (NULL != keyPressQueue) {
        /* Take from the front */
        ret = keyPressQueue->front;

        if (NULL != ret) {
            /* Update the queue - remove the front most element */
            keyPressQueue->front = keyPressQueue->front->next;
        }
    }

    return ret;
}

typedef enum USB_STAGES {
    ENUMERATION_STAGE,
    HID_STAGE
} USBStage;

/****************** Globals ******************/
KeyPressQ* keyQueue = NULL;
USBStage   curStage = ENUMERATION_STAGE;

/* Function Prototypes */
void Interrupt_Init(void);
void USART_Init(unsigned int baud_rate);
void USBController_Init(void);
void await_IN_packet(void);
void read_from_host(UBYTE *data, int num_read);
void usb_load_data(UBYTE *data, int num_write);
void usb_send_data(void);
void send_string_desc_to_host(int desc_num);
void send_config_descriptor_to_host(void);
void flash_error(int code);
void configure_control_endpoint(void);
void configure_hid_endpoints(void);

#define SET_IO_PIN(PORT_L, PIN_N) ((PORT ## PORT_L) |= (1 << (PORT ## PORT_L ## PIN_N)))
#define CLEAR_IO_PIN(PORT_L, PIN_N) ((PORT ## PORT_L) &= ~(1 << (PORT ## PORT_L ## PIN_N)))

#define IO_PIN_HIGH(PORT_L, PIN_N) ((PIN ## PORT_L) & (1 << PIN_N))
#define IO_PIN_LOW(PORT_L, PIN_N) (!((PIN ## PORT_L) & (1 << PIN_N)))


#define CONFIGURE_IN_IO_PIN(P, PIN) \
    ( \
        (DDR ## P) &= ~(1 << (DD ## P ## PIN)), \
        (PORT ## P) |= (1 << (PORT ## P ## PIN)) \
    )
#define CONFIGURE_OUT_IO_PIN(P, PIN) ((DDR ## P) |= (1 << (DD ## P ## PIN)))

int main(void) {
    io_pins_init();
    //Interrupt_Init();
    //USART_Init(BAUD_RATE(9600));
    USBController_Init();
    
    while (INFINITE) {

        _NOP(); // nop for synchronization
        
        if (IO_PIN_LOW(E, 6)) {
            SET_IO_PIN(D, 6); 
            hidReport.key1Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(D, 6);
            hidReport.key1Val = B_FALSE;
        }

        //_NOP(); // nop for synchronization
        if (IO_PIN_LOW(D, 0)) {
            SET_IO_PIN(C, 7);
            hidReport.key2Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(C, 7);
            hidReport.key2Val = B_FALSE;
        }

#if 1
       // _NOP(); // nop for synchronization
        if (IO_PIN_LOW(D, 1)) {
            SET_IO_PIN(D, 7);
            hidReport.key3Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(D, 7);
            hidReport.key3Val = B_FALSE;
        }

        if (IO_PIN_LOW(D, 2)) {
            SET_IO_PIN(C, 6);
            hidReport.key4Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(C, 6);
            hidReport.key4Val = B_FALSE;
        }

        if (IO_PIN_LOW(D, 3)) {
            SET_IO_PIN(D, 4);
            hidReport.key5Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(D, 4);
            hidReport.key5Val = B_FALSE;
        }

        if (IO_PIN_LOW(B, 7)) {
            SET_IO_PIN(F, 7);
            hidReport.key6Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(F, 7);
            hidReport.key6Val = B_FALSE;
        }

        if (IO_PIN_LOW(B, 6)) {
            SET_IO_PIN(F, 6);
            hidReport.key7Val = B_TRUE;
        }
        else {
            CLEAR_IO_PIN(F, 6);
            hidReport.key7Val = B_FALSE;
        }

        if (IO_PIN_LOW(B, 5)) {
            SET_IO_PIN(F, 5);
        }
        else {
            CLEAR_IO_PIN(F, 5);
        }

        if (IO_PIN_LOW(B, 4)) {
            SET_IO_PIN(F, 4);
        } 
        else {
            CLEAR_IO_PIN(F, 4);
		}
#endif

		_delay_ms(DELAY_CONST);
    }

    return RET_OK;
}

void io_pins_init(void) {
    // Keys
	CONFIGURE_IN_IO_PIN(D, 0);
    CONFIGURE_IN_IO_PIN(E, 6);
    CONFIGURE_IN_IO_PIN(D, 1);
    CONFIGURE_IN_IO_PIN(D, 2);
    CONFIGURE_IN_IO_PIN(D, 3);
    CONFIGURE_IN_IO_PIN(B, 7); // problem
    CONFIGURE_IN_IO_PIN(B, 6);

    // start / fx keys
    CONFIGURE_IN_IO_PIN(B, 5);
    CONFIGURE_IN_IO_PIN(B, 4);

    // keys leds
    CONFIGURE_OUT_IO_PIN(D, 6);
    CONFIGURE_OUT_IO_PIN(C, 7);
    CONFIGURE_OUT_IO_PIN(D, 7);
    CONFIGURE_OUT_IO_PIN(C, 6);
    CONFIGURE_OUT_IO_PIN(D, 4);
    CONFIGURE_OUT_IO_PIN(F, 7);
    CONFIGURE_OUT_IO_PIN(F, 6);

    // start / fx keys leds
    CONFIGURE_OUT_IO_PIN(F, 5);
    CONFIGURE_OUT_IO_PIN(F, 4);
}

typedef enum PIN_DIRECTION {
    PIN_INPUT,
    PIN_OUTPUT
} PIN_DIR_T;

#define IO_PIN_CONFIG(PORTL, PIN, INT_REG, REG_BIT)

void Interrupt_Init(void) {

    //CONFIGURE_IO_PIN(E, 6, PIN_INPUT, B_TRUE);

#if 0
    /* Enable pull-up resistor on PINE6 for well defined level */
    PORTE |= (1 << PORTE6);

    /* Setup PINE6 as input */
    DDRE &= ~(0 << DDE6);
#endif

    //CONFIGURE_IO_PIN(D, 6, PIN_OUTPUT, B_FALSE);

#if 0
    /* Set output High */
    PORTD |= (1 << PORTD6);

    /* Setup PIND6 as output (LED) */
    DDRD |= (1 << DDD6);
#endif

    /* Enable Global Interrupts SEREG --> 'L' bit ON */
    sei();

    /* Enable INT6 interrupt */
    EIMSK |= (1 << INT6);
}

void USART_Init(unsigned int baud_rate) {
    /* Clear interrupt flags before initializing USART */
    cli();

    UBRR1H = (unsigned char)(baud_rate >> 8);
    UBRR1L = (unsigned char)(baud_rate);

    /* Enable receiver and transmitter */
    UCSR1B = (1 << TXEN1);

    /* Set frame format: 8data, no parity, 1stop bit */
    UCSR1C = (3 << UCSZ10);

    sei();
}

/************************************************************************
 * What is an endpoint?
 *   -> buffer on USB device
 ************************************************************************/
void USBController_Init(void) {
    /* Disable global interrupts???? */
    cli();

    /* 1. Power on USB pads regulator */
    UHWCON |= (1 << UVREGE);

    /* 2. Configure PLL interface */
    PLLCSR |= (1 << PINDIV); /* Using 16MHz clock source */
    //PLLFRQ |= (1 << PDIV2);
    PLLFRQ |= (0 << PINMUX);

    /* 3. Enable PLL */
    PLLCSR |= (1 << PLLE);

    /* 4. Check PLL lock */
    while (!(PLLCSR & (1 << PLOCK)))
        ;

    /* 5. Enable USB interface - device should be in "Device" state and should be 'idle' */
    USBCON |= (1 << USBE);

    /* 6. Configure USB interface (speed, endpoints config) */
    UDCON &= ~(1 << LSM);    // Low-Speed Mode
    USBCON &= ~(1 << FRZCLK);  // Disable freeze clock - why???

    /* Enable the VBUS pad in order for the mcu to attach to the USB and the host to recognize it */
    USBCON |= (1 << OTGPADE);

    UDIEN |= (1 << EORSTE); /* Enable End-of-Reset interrupts */
    UDIEN |= (1 << UPRSME);
    UDIEN |= (1 << EORSME);
    UDIEN |= (1 << SOFE);
    UDIEN |= (1 << WAKEUPE);
    UDIEN |= (1 << SUSPE);

    /* Configure the Control endpoint */
    configure_control_endpoint();
    configure_hid_endpoints();

    /* 7. Wait for USB VBUS info connection */
    while (!(USBSTA & (1 << VBUS)))
        ;

    /* 8. Attach USB device */
    UDCON &= ~(1 << DETACH);

    /* enable global interrupts */
    sei();
}

void await_IN_packet(void) {
    while (!(UEINTX & (1 << TXINI)))
        ;
}

void await_OUT_packet(void) {
    while (!(UEINTX & (1 << RXOUTI)))
        ;
}

/* Might need a queue to store the data - must ensure that we've received the IN
 signal from the host but we can't be waiting on it in here */
ISR(INT6_vect) {
    /* Disable INT6 interrupt */
    EIMSK &= ~(1 << INT6);

    /* Turn ON output LED PIND6 */
    SET_IO_PIN(D, 6);
    //PORTD |= (1 << PORTD6);
    /* If the UDRE1 field in the UCSR1A is set then it's okay to transmit the data */
    //while (!(UCSR1A & (1 << UDRE1)));
    //enqueue(&keyQueue, 0x04);
    //UDR1 = 97;

    /* Enable INT6 interrupt */
    EIMSK |= (1 << INT6);
}


#define SELECT_ENDPOINT(n) (UENUM = n)
#define CONTROL_ENDPOINT_TYPE (3)

void configure_hid_endpoints(void) {
    SELECT_ENDPOINT(UENUM_1);
    UECONX |= (1 << EPEN);
    UECFG0X |= (1 << EPTYPE0) | (1 << EPTYPE1) | (1 << EPDIR);
    UECFG1X &= ~((1 << EPSIZE2) | (1 << EPSIZE1) | (1 << EPSIZE0) | (1 << EPBK0) | (1 << EPBK0));
    UECFG1X |= (1 << ALLOC);
    
    while (!(UESTA0X & (1 << CFGOK)));

#if NO_OUT_EP
    SELECT_ENDPOINT(UENUM_2);
    UECONX |= (1 << EPEN);
    UECFG0X |= (1 << EPTYPE0) | (1 << EPTYPE1);
    UECFG0X &= ~(1 << EPDIR);
    UECFG1X &= ~((1 << EPSIZE2) | (1 << EPSIZE1) | (1 << EPSIZE0) | (1 << EPBK0) | (1 << EPBK0));
    UECFG1X |= (1 << ALLOC);
#endif
}

void configure_control_endpoint(void) {
    /* Configure the Control endpoint */
    SELECT_ENDPOINT(CONTROL_ENDPOINT); /* Select the specified endpoint */
    UECONX |= (1 << EPEN); /* Enable the control endpoint */
    UECFG0X = 0;                  //CONTROL_ENDPOINT_CONFIG;
    UECFG1X = (0 << EPSIZE2) | (0 << EPSIZE1) | (0 << EPSIZE0) | (0 << EPBK0) |
              (0 << EPBK1) | (1 << ALLOC); /* 8 bytes, one bank, allocate */

    /* Enable relevant endpoint interrupts (USB_COM_vect) */
    //UEIENX |= (1 << TXINE);    /* Enable IN packet interrupts */
    //UEIENX |= (1 << RXOUTE);   /* Enable OUT packet interrupts */
    UEIENX |= (1 << RXSTPE); /* Enable SETUP Packet interrupts */
    //UEIENX |= (1 << FLERRE);   /* Enable Flow Error interrupts */
    //UEIENX |= (1 << NAKINE);   /* Enable NAK IN handshake packet interrupts */
    //UEIENX |= (1 << NAKOUTE);  /* Enable NAK OUT handshake packet interrupts */
    //UEIENX |= (1 << STALLEDE); /* Enable STALLED interrupts */
}

#define START_OF_FRAME_INTERRUPT  (UDINT & (1 << SOFI))
#define END_OF_RESET_INTERRUPT    (UDINT & (1 << EORSTI))
#define UPSTREAM_RESUME_INTERRUPT (UDINT & (1 << UPRSMI))
#define END_OF_RESUME_INTERRUPT   (UDINT & (1 << EORSMI))
#define WAKEUP_INTERRUPT          (UDINT & (1 << WAKEUPI))
#define SUSPEND_INTERRUPT         (UDINT & (1 << SUSPI))
ISR(USB_GEN_vect) {
    /*
     * Relevant registers:
     *  ========== Interrupt | Interrupt Enable =========
     *  - USBINT -> VBUSTI  [0] <- USBCON [0] (VBUSTE)
     *  - UDINT  -> UPRSMI  [6] <- UDIEN  [6] (UPRSME)
     *  - UDINT  -> EORSMI  [5] <- UDIEN  [5] (EORSME)
     *  - UDINT  -> WAKEUPI [4] <- UDIEN  [4] (WAKEUPE)
     *  - UDINT  -> EORSTI  [3] <- UDIEN  [3] (EORSTE)
     *  - UDINT  -> SOFI    [2] <- UDIEN  [2] (SOFE)
     *  - UDINT  -> SUSPI   [0] <- UDIEN  [0] (SUSPE)
     *
     * "Almost all these interrupts are time-relative events that will be detected only
     * if the USB clock is enabled (FRZCLK bit set)" - ???????????
     */

     /* Need to handle a USB reset */
     /* flash_error(GEN_INT_VECT_CODE); */

     /* Seems that SOFIs don't really need to be handled but "shall be cleared by firmware" */
    if (START_OF_FRAME_INTERRUPT) {
        UDINT &= ~(1 << SOFI);
        /*flash_error(START_OF_FRAME_INT_CODE);*/
    }

    /* USB Resets will reset the endpoint configurations - need to reconfigure */
    if (END_OF_RESET_INTERRUPT) {
        //quick_flash();
        configure_control_endpoint();
        configure_hid_endpoints();
        UDINT &= ~(1 << EORSTI);
    }

    if (UPSTREAM_RESUME_INTERRUPT) {
        UDINT &= ~(1 << UPRSMI);
        /*flash_error(UPSTREAM_RESUME_INT_CODE);*/
    }

    if (END_OF_RESUME_INTERRUPT) {
        UDINT &= ~(1 << EORSMI);
        /*flash_error(END_OF_RESUME_INT_CODE);*/
    }

    if (WAKEUP_INTERRUPT) {
        UDINT &= ~(1 << WAKEUPI);
        /*flash_error(WAKEUP_INT_CODE);*/
    }

    if (SUSPEND_INTERRUPT) {
        UDINT &= ~(1 << SUSPI);
        /*flash_error(SUSPEND_INT_CODE);*/
    }
}

#define RECEIVED_SETUP_PACKET (UEINTX & (1 << RXSTPI))
#define RECEIVED_IN_PACKET    (UEINTX & (1 << TXINI))
#define RECEIVED_OUT_PACKET   (UEINTX & (1 << RXOUTI))
#define NAKINI_SENT           (UEINTX & (1 << NAKINI))
#define ACK_IN_PACKET         (UEINTX &= ~(1 << TXINI))
#define ACK_OUT_PACKET        (UEINTX &= ~(1 << RXOUTI))
#define ACK_SETUP_PACKET      (UEINTX &= ~(1 << RXSTPI))

#define STRING_DESCRIPTOR_REQ (setupPacket.wValueL)
#define DESCRIPTOR_TYPE       (setupPacket.wValueH)

/*
 * Signals sent to endpoints (control included)
 */
ISR( USB_COM_vect ) {
    /* Relevant registers:
     * ========== Interrupt | Interrupt Enable =========
     *  - OVERFI   -> UESTAX [6] <- UEIENX [7] (FLERRE)
     *  - UNDERFI  -> UESTAX [5] <- UEIENX [7] (FLERRE)
     *  - NAKINI   -> UEINTX [5] <- UEIENX [6] (NAKINE)
     *  - NAKOUTI  -> UEINTX [4] <- UEIENX [4] (TXSTPE)
     *  - RXSTPI   -> UEINTX [3] <- UEIENX [3] (RXSTPE)
     *  - RXOUTI   -> UEINTX [2] <- UEIENX [2] (RXOUTE)
     *  - STALLEDI -> UEINTX [1] <- UEIENX [1] (STALLEDE)
     *  - TXINI    -> UEINTX [0] <- UEIENX [0] (TXINE)
     */
     /*flash_error(COM_INT_VECT_CODE);*/
    int bytesInBuf = 0;
    SELECT_ENDPOINT(CONTROL_ENDPOINT);

    if (RECEIVED_SETUP_PACKET) {
        //flash_error(RECEIVED_SETUP_INT_CODE);
        /* Read the data packet from the host */
        read_from_host((UBYTE*)&setupPacket, sizeof(USBSetupPacket));

        /* Before preceding make sure endpoints are flushed out so we're working with a clean slate */

        /* Acknowledge the packet */
        ACK_SETUP_PACKET; /* This will acknowledge the setup packet and clear the endpoint bank */

        if (REQUEST_TYPE_GET(setupPacket.bmRequestType) == STANDARD) {
            /* Get the request type */
            switch (setupPacket.bRequest) {
            case GET_DESCRIPTOR:
                /* If wIndex is not 0 then we have a String Descriptor Request
                 * on our hands. */
                switch (DESCRIPTOR_TYPE) {
                case HID_REPORT:
                    usb_load_data((UBYTE *)hidReportDescriptor, hidReportDescriptorSize);
                    if (PACKET_SIZE == g_numBytesInControlEP) {
                        usb_send_data();
                    }
                    usb_send_data();
                    
                    SELECT_ENDPOINT(UENUM_1);
                    UEIENX |= (1 << TXINE);
                    curStage = HID_STAGE;
                    break;

                case DEVICE_DESC:
                    usb_load_data(
                        (UBYTE*)&deviceDescriptor,
                        deviceDescriptor.bLength
                    );
                    usb_send_data();
                    break;

                case CONFIG_DESC:
                    send_config_descriptor_to_host();
                    break;

                case STRING_DESC:
                    switch (STRING_DESCRIPTOR_REQ) {
                    case ZERO_IDX:
                        send_string_desc_to_host(ZERO_IDX);
                        break;

                    case MANUFACTURER_IDX:
                        send_string_desc_to_host(MANUFACTURER_IDX);
                        break;

                    case PRODUCT_IDX:
                        send_string_desc_to_host(PRODUCT_IDX);
                        break;

                    case SERIAL_IDX:
                        send_string_desc_to_host(SERIAL_IDX);
                        break;

                    default:
                        /* Something didn't go right */
                        break;
                    }
                    break;

                default:
                    break;
                }

                break;

            case SET_ADDRESS:
                /* Must complete STATUS stage within 50ms */
                /* Store the received address but keep address enable bit cleared */
                /* 0111 1111 & XXXX XXXX --> 0XXX XXXX */
                UDADDR = (~(1 << ADDEN) & setupPacket.wValueL);

                while (!RECEIVED_IN_PACKET)
                    ;
                /* Now we have to send an IN Zero Length Packet (ZLP) */
                /* send IN command of 0 bytes (IN 0 Zero length packet) */
                /* How do i do this?  ---> CLEAR TXINI TO HANDSHAKE (send IN packet 0 bytes) */
                /* Send IN command of 0 bytes */
                ACK_IN_PACKET;

                /* We're awaiting an IN packet from the host */
                while (!RECEIVED_IN_PACKET)
                    ;

                /* Enable address bit - ready to rock 'n' roll */
                UDADDR |= (1 << ADDEN);
                break;

            case SET_CONFIGURATION:
                // select configuration based on wValue
                usb_send_data();
#if NO_OUT_EP
                SELECT_ENDPOINT(UENUM_2);
                UEIENX |= (1 << RXOUTE);   /* Enable OUT packet interrupts */
#endif
                break;

            default:
                break;
            }
        }

        else if (REQUEST_TYPE_GET(setupPacket.bmRequestType) == CLASS) {
            if (setupPacket.bRequest == HID_SET_IDLE) {
                usb_send_data();
            }
        }
    }
    else if (RECEIVED_IN_PACKET && (HID_STAGE == curStage)) {
        UBYTE keyMask = (hidReport.key1Val << 0) | (hidReport.key2Val << 1) |
                        (hidReport.key3Val << 2) | (hidReport.key4Val << 3) |
                        (hidReport.key5Val << 4) | (hidReport.key6Val << 5) |
                        (hidReport.key7Val << 6);
        UBYTE xAxisVal = 0;
        UBYTE yAxisVal = 0;
        UBYTE zAxisVal = 0;

        SELECT_ENDPOINT(UENUM_1);
        usb_load_data(&(keyMask), sizeof(keyMask));
        usb_load_data(&(xAxisVal), sizeof(xAxisVal));
        usb_load_data(&(yAxisVal), sizeof(yAxisVal));
        usb_send_data();
        
    }
}

void read_from_host(UBYTE *data, int num_read) {
    int i;

    for (i = 0; i < num_read; i++) {
        *(data++) = UEDATX;
    }
}

void usb_load_data(UBYTE *data, int num_write) {
    int i;
    
    if (CONTROL_ENDPOINT == UENUM) {
        /* Ensure we are not attempting to write more than
         * what the host specified */
        if (num_write > setupPacket.wLength) {
            num_write = setupPacket.wLength;
        }
    }
    

    //flash_error(g_numBytesInControlEP);

    for (i = 0; i < num_write; i++) {
        /* If we reach the maximum packet size then send the packet and
         * clear the endpoint buffer */
        if (g_numBytesInControlEP >= PACKET_SIZE) {
            usb_send_data();
        }

        /* Check to make sure the FIFO buffer is
         * free and able to accept data */
        while (!RECEIVED_IN_PACKET);

        /* Move a byte of data to be transfered to the FIFO endpoint buffer */
        UEDATX = *(data++);
        g_numBytesInControlEP++;
    }
}

void usb_send_data(void) {
    while (!RECEIVED_IN_PACKET);
    ACK_IN_PACKET;
    g_numBytesInControlEP = 0;
    if (UENUM_1 == UENUM) {
        UEINTX &= ~(1 << FIFOCON);
    }
}

void send_config_descriptor_to_host(void) {
    UBYTE data[D_CON + D_INT + D_END + D_HID];
    int i = 0;

    data[i++] = configDescriptor.bLength;
    data[i++] = configDescriptor.bDescriptorType;
    data[i++] = configDescriptor.wTotalLength;
    data[i++] = (configDescriptor.wTotalLength >> PACKET_SIZE);
    data[i++] = configDescriptor.bNumInterfaces;
    data[i++] = configDescriptor.bConfigurationValue;
    data[i++] = configDescriptor.iConfiguration;
    data[i++] = configDescriptor.bmAttributes;
    data[i++] = configDescriptor.bMaxPower;

    data[i++] = interfaceDescriptor.bLength;
    data[i++] = interfaceDescriptor.bDescriptorType;
    data[i++] = interfaceDescriptor.bInterfaceNumber;
    data[i++] = interfaceDescriptor.bAlternateSetting;
    data[i++] = interfaceDescriptor.bNumEndpoints;
    data[i++] = interfaceDescriptor.bInterfaceClass;
    data[i++] = interfaceDescriptor.bInterfaceSubClass;
    data[i++] = interfaceDescriptor.bInterfaceProtocol;
    data[i++] = interfaceDescriptor.bInterface;

    data[i++] = hidDescriptor.bLength;
    data[i++] = hidDescriptor.bDescriptorType;
    data[i++] = hidDescriptor.bcdHID;
    data[i++] = (hidDescriptor.bcdHID >> PACKET_SIZE);
    data[i++] = hidDescriptor.bCountryCode;
    data[i++] = hidDescriptor.bNumDescriptors;
    data[i++] = hidDescriptor.bReportDescriptorType;
    data[i++] = hidDescriptor.wDescriptorLength;
    data[i++] = (hidDescriptor.wDescriptorLength >> PACKET_SIZE);

    data[i++] = endpointInDescriptor.bLength;
    data[i++] = endpointInDescriptor.bDescriptorType;
    data[i++] = endpointInDescriptor.bEndpointAddress;
    data[i++] = endpointInDescriptor.bmAttributes;
    data[i++] = endpointInDescriptor.wMaxPacketSize;
    data[i++] = (endpointInDescriptor.wMaxPacketSize >> PACKET_SIZE);
    data[i++] = endpointInDescriptor.bInterval;

#if NO_OUT_EP
  data[i++] = endpointOutDescriptor.bLength;
  data[i++] = endpointOutDescriptor.bDescriptorType;
  data[i++] = endpointOutDescriptor.bEndpointAddress;
  data[i++] = endpointOutDescriptor.bmAttributes;
  data[i++] = endpointOutDescriptor.wMaxPacketSize;
  data[i++] = (endpointOutDescriptor.wMaxPacketSize >> PACKET_SIZE);
  data[i++] = endpointOutDescriptor.bInterval;
#endif

  usb_load_data(data, sizeof(data));
  if (g_numBytesInControlEP >= PACKET_SIZE) {
      usb_send_data();
  }
  usb_send_data();
}

#define STR_DESC_GEN_PACKET_INFO_SIZE 2 /* size of attributes that give general information about
                                         * the packet (length and type) */
void send_string_desc_to_host(int desc_num) {
    int i;

    switch (desc_num) {
    case ZERO_IDX: // str desc 0
        usb_load_data((UBYTE*)&stringDescriptorZero, STR_DESC_GEN_PACKET_INFO_SIZE);
        for (i = 0; i < LANG_ID_LEN; i++) {
            usb_load_data(
                (UBYTE *)&(stringDescriptorZero.wLANGID[i]), 
                sizeof(stringDescriptorZero.wLANGID[i])
            );
        }
        break;

    case MANUFACTURER_IDX: // manufact str desc
        usb_load_data((UBYTE*)&manufacturerDescriptor, STR_DESC_GEN_PACKET_INFO_SIZE);
        usb_load_data((UBYTE*)manufacturerDescriptor.bString, strlen(manufacturerDescriptor.bString));
        break;

    case PRODUCT_IDX: // prod str desc
        usb_load_data((UBYTE*)&productDescriptor, STR_DESC_GEN_PACKET_INFO_SIZE);
        usb_load_data((UBYTE*)productDescriptor.bString, strlen(productDescriptor.bString));
        break;

    case SERIAL_IDX: // serial str desc
        usb_load_data((UBYTE*)&serialDescriptor, STR_DESC_GEN_PACKET_INFO_SIZE);
        usb_load_data((UBYTE*)serialDescriptor.bString, strlen(serialDescriptor.bString));
        break;
    }

    if (g_numBytesInControlEP >= PACKET_SIZE) {
        usb_send_data();
    }
    usb_send_data();
}

void flash_error(int code) {
  int i;

  led_on();
  _delay_ms(1950);
  led_off();
  _delay_ms(1950);

  for (i = 0; i < code; i++) {
    led_on();
    led_off();
  }
}

void led_on(void) {
  PORTD |= (1 << PORTD6);
  _delay_ms(250);
}

void led_off(void) {
  PORTD &= ~(1 << PORTD6);
  _delay_ms(250);
}

void quick_flash(void) {
  PORTD |= (1 << PORTD6);
  _delay_ms(100);
  PORTD &= ~(1 << PORTD6);
}

#include "Descriptor.h"

#define USB_HID_CLASS 0x03
#define CONTROL_ENDPOINT_CONFIG (~((1 << EPTYPE0) | (1 << EPTYPE1) | (1 << EPDIR)))


unsigned char          USB_CONFIG = 0;
volatile unsigned char USB_CURRENT_STATUS = 0;
USB_SETUP              setupPacket;

#define MANUFACT_DESC_STR "test manufacturer"
#define PROD_DESC_STR     "test product"
#define SER_DESC_STR      "test serial 0123456789"
volatile unsigned int g_numBytesInControlEP = 0;

/* These could be stored somewhere a little more permanent */
#define NO_DEVICE_CLASS    0x00
#define NO_DEVICE_SUBCLASS 0x00
#define NO_INTERF_PROTOCOL 0x00

const DevDesc deviceDescriptor = {
	sizeof(DevDesc),  /* total size in bytes, maybe just use sizeof? */
	DEVICE_DESC,
	USB1_1,
	NO_DEVICE_CLASS,
	NO_DEVICE_SUBCLASS,
	NO_INTERF_PROTOCOL,
	PACKET_SIZE,
	VENDOR_ID,
	PRODUCT_ID,
	SOFT_VERSION,
	MANUFACTURER_IDX, /* iManufacturer */
	PRODUCT_IDX,      /* iProduct */
	SERIAL_IDX,       /* iSerialNumber */
	1                 /* Just one configuration */
};

#define BUS_POWERED (0x20)
#define NUM_INTERF  (0x01)
const ConDesc configDescriptor = {
  sizeof(ConDesc), /* size */
  CONFIG_DESC,
  sizeof(ConDesc) + sizeof(IntDesc) + sizeof(EndDesc) + sizeof(HID_Desc), /* Total length of entire hierarchy/structure */
  NUM_INTERF, /* 1 interface */
  1, /* this config selected */
  0, /* no string descriptor */
  BUS_POWERED, /* powered by bus */
  MAX_POWER
};

#define USB_INT_NO_SUBCLASS    0x00
#define USB_INT_BOOT_INTERF    0x01
#define USB_INT_NO_STRING_DESC 0x00

/* ||||||||||| Might be OK ||||||||||| */
#define NO_PROTOCOL 0x00
#define BOOT_INTERF_NOT_SUPPORTED 0x00
const IntDesc interfaceDescriptor = {
	sizeof(IntDesc),
	INTERFACE_DESC,
	0, /* interface number (0-based) */
	0, /* Alternate setting ???? */
	1, /* One endpoint - IN used for sending button presses to host */
	USB_HID_CLASS, /* Interface class - HID */
	BOOT_INTERF_NOT_SUPPORTED, /* No subclass ?? - seems like it | Update: trying boot interface | boot interface no more */
	NO_PROTOCOL, /* no protocol ?? - seems like it */
	USB_INT_NO_STRING_DESC  /* no string descriptor */
};

#define HID_REPORT_CONST_SIZE 25

#if 0
const UBYTE hidReportDescriptor[] = {
	HID_USAGE_PAGE_KEY,     HID_USAGE_PAGE_GEN_DESK_CTRLS_VAL,
	HID_USAGE_KEY,          HID_USAGE_GAMEPAD_VAL,
	HID_COLLECTION_KEY,     HID_COLLECITON_APP_VAL,
	HID_REPORT_ID_KEY,      HID_REPORT_ID_VAL, // Report ID: 1
	HID_USAGE_PAGE_KEY,     HID_USAGE_PAGE_BUTTON_VAL,
	HID_USAGE_MIN_KEY,      0x01,
	HID_USAGE_MAX_KEY,      0x07,
	HID_LOG_MIN_KEY,        0x00,
	HID_LOG_MAX_KEY,        0x01,
	HID_REPORT_SIZE_KEY,    0x01, // Report Size: 1
	HID_REPORT_COUNT_KEY,   0x07, // Report Count: 7
	HID_INPUT_KEY,          0x02, // Input: Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position
	HID_REPORT_SIZE_KEY,    0x01, // Report Size: 1 (Padding)
	HID_REPORT_COUNT_KEY,   0x01, // Report Count: 1 (Padding)
	HID_INPUT_KEY,          0x03, // Input: Const,Var,Abs
	HID_END_COLLECTION_KEY
};
#else 
const UBYTE hidReportDescriptor[] = {
	HID_USAGE_PAGE_KEY,     HID_USAGE_PAGE_GEN_DESK_CTRLS_VAL,
	HID_USAGE_KEY,          HID_USAGE_GAMEPAD_VAL,
	HID_COLLECTION_KEY,     HID_COLLECITON_APP_VAL,
    //HID_COLLECTION_KEY,     HID_COLLECTION_REP_VAL,
	//HID_REPORT_ID_KEY,      HID_REPORT_ID_VAL, // Report ID: 1
	HID_USAGE_PAGE_KEY,     HID_USAGE_PAGE_BUTTON_VAL,
	HID_USAGE_MIN_KEY,      0x01,
	HID_USAGE_MAX_KEY,      0x07,
	HID_LOG_MIN_KEY,        0x00,
	HID_LOG_MAX_KEY,        0x01,
	HID_REPORT_SIZE_KEY,    0x01, // Report Size: 1
	HID_REPORT_COUNT_KEY,   0x07, // Report Count: 7
	HID_INPUT_KEY,          0x02, // Input: Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position
	HID_REPORT_SIZE_KEY,    0x01, // Report Size: 1 (Padding)
	HID_REPORT_COUNT_KEY,   0x01, // Report Count: 1 (Padding)
	HID_INPUT_KEY,          0x03, // Input: Const,Var,Abs (Padding)
	HID_USAGE_PAGE_KEY,     HID_USAGE_PAGE_GEN_DESK_CTRLS_VAL,
	HID_USAGE_KEY,          HID_USAGE_X,
	HID_USAGE_KEY,          HID_USAGE_Y,
	HID_LOG_MIN_KEY,        0x81, /* -127 */
	HID_LOG_MAX_KEY,        0x7F, /* 127 */
    HID_REPORT_COUNT_KEY,   0x02, /* Report Count of 3 */
	HID_REPORT_SIZE_KEY,    0x08, /* Report Size of 8 (bits) */
	HID_INPUT_KEY,          0x03, // Input: Data,Var,Abs
	//HID_END_COLLECTION_KEY,
    HID_END_COLLECTION_KEY
};
#endif
const size_t hidReportDescriptorSize = sizeof(hidReportDescriptor) / sizeof(UBYTE);

HID_Desc hidDescriptor = {
	sizeof(HID_Desc),
	HID_DESC,
	HID1_1,
	HID_NO_COUNTRY_CODE,
	1, // 1 descriptor (report descriptor)
	USB_INT_REPORT,
	sizeof(hidReportDescriptor)
};

HID_Rep hidReport = {
	HID_REPORT_ID_VAL,
	0
};

#define INTERRUPT_IN (0x03)
#define INTERRUPT_OUT (0x03)
#define ENDPOINT_IN(endpoint_num) (0x80 + endpoint_num)
#define ENDPOINT_OUT(endpoint_num) (0x00 + endpoint_num)
const EndDesc endpointInDescriptor = {
	sizeof(EndDesc),
	ENDPOINT_DESC,
	ENDPOINT_IN(UENUM_1), /* Endpoint number - IN (0x80) + endpoint 1 (0x01) */
	INTERRUPT_IN,         /* Interrupt comms */
	PACKET_SIZE,          /* packet size */
	25                    /* poll every 25 frames (ms in low speed) */
};

const EndDesc endpointOutDescriptor = {
	sizeof(EndDesc),
	ENDPOINT_DESC,
	ENDPOINT_OUT(UENUM_2),
	INTERRUPT_OUT,
	PACKET_SIZE,
	25
};

#define STRING_DESCRIPTOR_ID 0x03
#define COMMON_STR_DESC_SIZE 2
const StrDesc manufacturerDescriptor = {
	COMMON_STR_DESC_SIZE + (sizeof(MANUFACT_DESC_STR) / sizeof(MANUFACT_DESC_STR[0])),
	STRING_DESCRIPTOR_ID,
	MANUFACT_DESC_STR
};

#define PROD_STR_DESC_SIZE 1
const StrDesc productDescriptor = {
	COMMON_STR_DESC_SIZE + (sizeof(PROD_DESC_STR) / sizeof(PROD_DESC_STR[0])),
	STRING_DESCRIPTOR_ID,
	PROD_DESC_STR
};

#define SER_STR_DESC_SIZE 1
const StrDesc serialDescriptor = {
	COMMON_STR_DESC_SIZE + (sizeof(SER_DESC_STR) / sizeof(SER_DESC_STR[0])),
	STRING_DESCRIPTOR_ID,
	SER_DESC_STR
};

#define ENG_CAN_LANG_ID 0x1009
#define ENG_USA_LANG_ID 0x0409
const StrDescZero stringDescriptorZero = {
	sizeof(StrDescZero),
	STRING_DESCRIPTOR_ID,
	{
		ENG_CAN_LANG_ID,
		ENG_USA_LANG_ID
	}
};

#if 0
void flush_endpoint(int endpoint_num) {
	int i;

	UENUM = endpoint_num;

	for (i = 0; i < g_numBytesInControlEP; i++) {
		UEDATX;
	}

	g_numBytesInControlEP = 0;
}
#endif

#if 0
// Should bitmasks be used? - maybe just internally but perhaps not for api/libraries?
void get_descriptor_list(UBYTE* data, D_SELECT descriptor_mask) {
	int total_size;

	if (descriptor_mask & (1 << DEVICE_DESC)) {
		//total_size = 
	}

	if (descriptor_mask & (1 << CONFIG_DESC)) {

	}

	if (descriptor_mask & (1 << INTERFACE_DESC)) {

	}

	if (descriptor_mask & (1 << ENDPOINT_DESC)) {

	}
}
#endif

#if 0
void test(int val) {
	int i = val;

	return;
}
#endif

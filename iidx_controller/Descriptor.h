/*
 * Descriptor.h
 *
 * Created: 9/3/2021 4:20:00 AM
 *  Author: nil
 */ 


#ifndef DESCRIPTOR_H_
#define DESCRIPTOR_H_

#include <avr/io.h>
#include <stdio.h>

#define NUM_BYTES_IN_ENDPOINT (((int)UEBCHX << 8) | (UEBCLX))

#define CONTROL_ENDPOINT UENUM_0
#define HID_IN_ENDPOINT  UENUM_1

/* RequestType bit mappings */
#define DATA_PHASE_TRANSFER_DIRECTION 7
#define TYPE1                         6
#define TYPE0                         5
#define RECIPIENT4                    4
#define RECIPIENT3                    3
#define RECIPIENT2                    2
#define RECIPIENT1                    1
#define RECIPIENT0                    0

/* Meanings */
enum DataPhaseTransferDirections {
    HOST_TO_DEVICE,
    DEVICE_TO_HOST
};

enum Types {
    STANDARD,
    CLASS,
    VENDOR
};

enum Recipients {
    DEVICE,
    INTERFACE,
    ENDPOINT,
    OTHER
};

/* Setup Request Packet Gets */
#define DATA_PHASE_TRANSFER_DIRECTION_GET(reg)  ((reg & (1 << DATA_PHASE_TRANSFER_DIRECTION)) >> \
                                                DATA_PHASE_TRANSFER_DIRECTION)
#define REQUEST_TYPE_GET(reg)                   ((reg & ((1 << TYPE0) | (1 << TYPE1))) >> TYPE0)
#define RECIPIENT_GET(reg)                      (reg & ((1 << RECIPIENT0) | (1 << RECIPIENT1) | \
                                                (1 << RECIPIENT2) | (1 << RECIPIENT3) | \
                                                (1 << RECIPIENT4)))

#define MAX_POWER 50
/* In 2mA so actual is 50mA */
/* Changed from 25 to 250 */
/* Caterina FW reports 0x32 or 50 which is 100mA */

typedef unsigned char D_SELECT;
typedef unsigned short T_UWORD;
typedef unsigned char T_UBYTE;

typedef unsigned char BOOL;

#define UBYTE unsigned char
#define UWORD unsigned short

#define HID_NO_COUNTRY_CODE 0

enum USB_DESCRIPTOR_SIZES {
  D_DEV = 18,
  D_CON = 9,
  D_INT = 9,
  D_END = 7,
  D_HID = 9
};

typedef enum USB_VERSIONS {
  USB1_0 = 0x0100,
  USB1_1 = 0x0110,
  USB2_0 = 0x0200
} USB_VER;

typedef enum USB_HID_VERSIONS {
    HID1_1 = 0x0110
} HID_VER;

enum STRING_DESCS_IDX {
  ZERO_IDX         = 0,
  MANUFACTURER_IDX = 1,
  PRODUCT_IDX      = 2,
  SERIAL_IDX       = 3
};

enum USB_DESCRIPTOR_CONSTANTS {
  BCD_USB           = USB2_0, /* USB version */
  DEVICE_CLASS      = 0x00,   /* HID - actually no? hmm (should be 0x00) */
  DEVICE_SUBCLASS   = 0x00,   /* ? - not needed anymore? */
  KEYBOARD_PROTOCOL = 0x01,
  INTER_PROTOCOL    = 0x00,   /* Keyboard? */
  VENDOR_ID         = 0x1209, /* Atmel ID - was 0x03EB - trying 0x2341 - trying logitech now (0x046D) - trying tecware now (0x0C45) - using test vid (0x1209) */
  PRODUCT_ID        = 0x0001, /* ATmega32u4  - was 0x2FF4 - trying 0x36 - trying keyboard now (0xC221) / k120 (0xC31C) - trying tecware now (0x652F) - test pid (0x0001)*/
  I_MANUFACTURER    = 0x01,
  I_PRODUCT         = 0x02,
  I_SERIAL          = 0x03,
  SOFT_VERSION      = 0x0001,
  PACKET_SIZE       = 8,
};

#if ALREADY_DEFINED
typedef enum USB_STANDARD_REQUEST_TYPES {
  GET_STATUS        = 0x00,
  CLEAR_FEATURE     = 0x01,
  SET_FEATURE       = 0x03,
  SET_ADDRESS       = 0x05,
  GET_DESCRIPTOR    = 0x06,
  SET_DESCRIPTOR    = 0x07,
  GET_CONFIGURATION = 0x08,
  SET_CONFIGURATION = 0x09
} USB_STD_REQ_TYPE;
#endif

typedef enum USB_REQUEST_TYPES {
  REQ_DEV_TO_HOST     = 0x80,
  REQ_HOST_TO_DEV_STD = 0x60,
  REQ_STD             = 0x00
} USB_REQ_TYPE;

typedef enum USB_REQUEST_TYPE_MASKS {
  GET_REQ_TYPE = 0x60
} USB_REQUEST_TYPE_MASK;

typedef enum USB_DESCRIPTOR_TYPES {
  DEVICE_DESC    = 0x01,
  CONFIG_DESC    = 0x02,
  STRING_DESC    = 0x03,
  INTERFACE_DESC = 0x04,
  ENDPOINT_DESC  = 0x05,
  HID_DESC       = 0x21,
  HID_REPORT     = 0x22
} USB_DESC_TYPE;

typedef struct myInterfaceDescriptor {
  unsigned char bLength;
  unsigned char bDescriptorType;
  unsigned char bInterfaceNumber;
  unsigned char bAlternateSetting;
  unsigned char bNumEndpoints;
  unsigned char bInterfaceClass;
  unsigned char bInterfaceSubClass;
  unsigned char bInterfaceProtocol;
  unsigned char bInterface;
} IntDesc;

typedef struct myConfigurationDescriptor {
  unsigned char  bLength;
  unsigned char  bDescriptorType;
  unsigned short wTotalLength;
  unsigned char  bNumInterfaces;
  unsigned char  bConfigurationValue;
  unsigned char  iConfiguration;
  unsigned char  bmAttributes;
  unsigned char  bMaxPower;
} ConDesc;

typedef struct myDeviceDescriptor {
  unsigned char  bLength;
  unsigned char  bDescriptorType;
  unsigned short bcdUSB;
  unsigned char  bDeviceClass;
  unsigned char  bDeviceSubClass;
  unsigned char  bDeviceProtocol;
  unsigned char  bMaxPacketSize;
  unsigned short idVendor;
  unsigned short idProduct;
  unsigned short bcdDevice;
  unsigned char  iManufacturer;
  unsigned char  iProduct;
  unsigned char  iSerialNumber;
  unsigned char  bNumConfigurations;
} DevDesc;

typedef struct myEndpointDescriptor {
  unsigned char  bLength;
  unsigned char  bDescriptorType;
  unsigned char  bEndpointAddress;
  unsigned char  bmAttributes;
  unsigned short wMaxPacketSize;
  unsigned char  bInterval;
} EndDesc;

typedef struct myStringDescriptor {
  unsigned char  bLength;
  unsigned char  bDescriptorType;
  char          *bString;
} StrDesc;

#define LANG_ID_LEN 2
typedef struct myStringDescriptorZero {
  unsigned char  bLength;
  unsigned char  bDescriptorType;
  unsigned short wLANGID[LANG_ID_LEN];
} StrDescZero;

typedef struct USBSetupPacket {
  unsigned char  bmRequestType;
  unsigned char  bRequest;
  unsigned char  wValueL; /* Low first? */
  unsigned char  wValueH;
  unsigned short wIndex;
  unsigned short wLength;
} USB_SETUP;

#define CLASS_GET_INTERFACE 10
#define HID_SET_IDLE        10

#define HID_USAGE_PAGE_KEY     0x05
#define HID_USAGE_KEY          0x09
#define HID_REPORT_ID_KEY      0x85
#define HID_USAGE_MIN_KEY      0x19
#define HID_USAGE_MAX_KEY      0x29
#define HID_COLLECTION_KEY     0xA1
#define HID_LOG_MIN_KEY        0x15
#define HID_LOG_MAX_KEY        0x25
#define HID_REPORT_SIZE_KEY    0x75
#define HID_REPORT_COUNT_KEY   0x95
#define HID_INPUT_KEY          0x81
#define HID_END_COLLECTION_KEY 0xC0

#define HID_USAGE_PAGE_GEN_DESK_CTRLS_VAL 0x01
#define HID_USAGE_PAGE_GAME_CTRLS_VAL     0x05
#define HID_USAGE_GAMEPAD_VAL             0x05
#define HID_USAGE_KEYBOARD_VAL            0x06
#define HID_USAGE_X                       0x30
#define HID_USAGE_Y                       0x31
#define HID_USAGE_Z                       0x32
#define HID_USAGE_PAGE_BUTTON_VAL         0x09
#define HID_COLLECITON_APP_VAL            0x01
#define HID_COLLECTION_REP_VAL            0x03
#define HID_REPORT_ID_VAL                 0x01


#if NOT_USING
typedef struct hidReportItem {
    union {
        struct {
            const UBYTE item;
            const UBYTE val;
        } keyVal;
        const UBYTE val;
    };
} HID_Item;
#endif

#define USB_INT_REPORT 0x22

typedef struct myHIDDescriptor {
  unsigned char  bLength;
  unsigned char  bDescriptorType;
  unsigned short bcdHID;
  unsigned char  bCountryCode;
  unsigned char  bNumDescriptors;
  unsigned char  bReportDescriptorType;
  unsigned short wDescriptorLength;
} HID_Desc;

typedef struct myHIDReport {
    UBYTE id;
    UBYTE key1Val;
    UBYTE key2Val;
    UBYTE key3Val;
    UBYTE key4Val;
    UBYTE key5Val;
    UBYTE key6Val;
    UBYTE key7Val;
    //UBYTE key8Val;
    //UBYTE key9Val;
} HID_Rep;

/* Globals */
extern unsigned char          USB_CONFIG;
extern volatile unsigned char USB_CURRENT_STATUS;
extern USB_SETUP              setupPacket;
extern const char            *stringDescriptors[];
extern volatile unsigned int  g_numBytesInControlEP;

extern const DevDesc     deviceDescriptor;
extern const ConDesc     configDescriptor;
extern const IntDesc     interfaceDescriptor;
extern       HID_Desc    hidDescriptor;
extern const UBYTE       hidReportDescriptor[];
extern       HID_Rep     hidReport;
extern const size_t      hidReportDescriptorSize;
extern const EndDesc     endpointInDescriptor;
extern const EndDesc     endpointOutDescriptor;
extern const StrDesc     manufacturerDescriptor;
extern const StrDesc     productDescriptor;
extern const StrDesc     serialDescriptor;
extern const StrDescZero stringDescriptorZero;

/* prototypes */
void flush_endpoint(int endpoint_num);
void test(int val);

#endif /* DESCRIPTOR_H_ */
